//NRP : 4210181008 & 4210181016.

#include <GL/glut.h> 
#include "game.h" //Memanggil library game.
#include <stdlib.h> //library ini digunakan untuk bagian score.
using namespace std;

#define COLUMNS 40 //Definisi jumlah kolom.
#define ROWS 40 //Definisi jumlah baris.
#define FPS 10 //Definisi FPS untuk animasi.

extern short sDirection; //Untuk dapat mengakses arah kepala snake menghadap.
bool gameOver = false; //Varaibel yang menyimpan data apakah game berakhir atau tidak.

int score = 0; //Variabel yang menyimpan score.

void timer_callback(int); //Inisialisasi fungsi timer yang digunakan untuk animasi.
void display_callback(); //Inisialisasi fungsi display yang akan memunculkan apapun yang digambar pada window.
void reshape_callback(int, int); //Inisialisasi fungsi yang mengatur ukuran window saat diubah-ubah sehingga semua perhitungannya tetap.
void keyboard_callback(int, int, int);

void init() //Fungsi inisialisasi yang akan mengatur apa yang ditampilkan.
{
	glClearColor(0.0, 0.0, 0.0, 1.0); //To change background color.
	initGrid(COLUMNS, ROWS); //Inisialisasi jumlah grid yang akan digambar pada layar.
}

int main(int argc, char** argv) //Fungsi main yang mengatur jalannya game.
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(500,500); //Ukuran window-nya adalah 500 kali 500.
	glutCreateWindow("SNAKE by Ulwan and Uncha"); //Nama game.
	glutDisplayFunc(display_callback); //Fungsi glut untuk memanggil fungsi menampilkan semua yang akan digambar pada layar.
	glutReshapeFunc(reshape_callback); //Fungsi glut untuk memanggil fungsi yang mengatur ukuran window.
	glutTimerFunc(0, timer_callback, 0); //Fungsi glut untuk memanggil fungsi timer yang digunakan untuk menganimasikan obyek.
	glutSpecialFunc(keyboard_callback); //Fungsi glut untuk memanggil fungsi inputan keyboard.
	init();
	glutMainLoop();
	return 0;
}

void display_callback() //Definisi fungsi display_callback yang berguna untuk menampilkan apa saja yang akan digambar pada window.
{
	glClear(GL_COLOR_BUFFER_BIT);
	drawGrid(); //Memanggil fungsi menggambarkan Map.
	drawSnake(); //Memanggil fungsi menggambarkan Snake.
	drawFood(); //Memanggil fungsi menggambarkan makanannya Snake.
	glutSwapBuffers();
	if (gameOver) //Untuk menampilkan score pada text box dan menutup game di saat game berakhir.
	{
		char _score[10]; //Karena MessageBox hanya menerima inputan char maka score di-convert dulu dari int ke char.
		_itoa_s(score, _score, 10); //Ini fungsi yang meng-convert-nya.
		char text[50] = "Your Score: ";
		strcat_s(text,_score); //Ini ketika score yang di-convert sudah disatukan dengan teks.
		MessageBox(NULL, text,"GAME OVER!", 0); //Funmgsi dari glut untuk menampilkan sebuah message / text box.
		exit(0); //Fungsi untuk menutup window.
	}
}

void reshape_callback(int w, int h) //Fungsi yang mengatur ukuran window dan projeksinya.
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, COLUMNS, 0.0, ROWS, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
}

void timer_callback(int) //Fungsi animasi seluruh yang akan dianimasikan.
{
	glutPostRedisplay();
	glutTimerFunc(1000/FPS, timer_callback, 0); //Mengulang memanggil fungsi display callback sepuluh kali setiap satu detik.
}

void keyboard_callback(int key, int, int) //Fungsi inputan keyboard.
{
	switch (key)
	{
	case GLUT_KEY_UP: //ketika yang ditekan adalah arrow up
		if (sDirection != DOWN) //dan arah hadap snake tidak ke bawah
			sDirection = UP; //maka snake akan menghadap ke atas
		break;
	case GLUT_KEY_DOWN: //Ketika yang ditekan adalah arrow down
		if (sDirection != UP) //dan snake tidak menghadap ke atas
			sDirection = DOWN; //snake akan menghadap ke bawah
		break;
	case GLUT_KEY_RIGHT: //Ketika yang ditekan adalah arrow kanan
		if (sDirection != LEFT) //dan snake tidak menghadap ke kiri
			sDirection = RIGHT; //snake akan menghadap ke kanan
		break;
	case GLUT_KEY_LEFT: //Ketika yang ditekan adalah arrow kiri
		if (sDirection != RIGHT) //dan snake tidak menghadap ke kanan
			sDirection = LEFT; //snake akan menghadap ke kiri
		break;
	}
}