//NRP : 4210181008 & 4210181016.

#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#define UP 1
#define DOWN -1
#define RIGHT 2
#define LEFT -2

#define MAX 60 //definisikan


//Library game yang digunakan untuk menghubungkan main.cpp dengan game.cpp
void initGrid(int, int); //definisik fungsi inisialisasi map / grid snake.
void drawGrid(); //definisi fungsi menggambar map / grid.
void drawSnake(); //definisi fungsi menggambar snake
void drawFood(); //definisi fungsi menggambar makanan
void random(int&, int&); //definisi fungsi random posisi makanan

#endif // GAME_H_INCLUDED