//NRP : 4210181008 & 4210181016.

#include <GL/glut.h>
#include <ctime> //Library untuk time.
#include "game.h" //memanggil library game.
using namespace std;

int gridX, gridY; //Deklarasikan variabel gridX untuk melambangkan baris, gridY untuk kolom.
int snake_length = 5; //Panjang snake di awal adalah lima kotak.
bool food = true; //Boolean yang berguna untuk mendeteksi apakah makanan sudah dimakan atau belum
int foodX, foodY; //Variabel posisi makanan
short sDirection = RIGHT; //Variabel arah hadap kepala snake
extern bool gameOver; //Memanggil variabel gameOver dari main.
extern int score; //Memanggil variabel score dari main.

int posX[60] = { 20,20,20,20,20 }, posY[60] = { 20,19,18,17,16 }; //Array posisi snake yang memiliki batas panjang sebanyak 60.

void  initGrid(int x, int y) //Fungsi inisialisasi variabel grid yang dibutuhkan untuk menggambar map dan mengatur posisi snake.
{
	gridX = x;
	gridY = y;
}

void unit(int, int);
void drawGrid() //fungsi menggambar map.
{
	for (int i = 0; i < gridX; i++) //akan digambar garis sebanyak 40 kali
	{
		for (int j = 0; j < gridY; j++) //dan kolom sebanyak 40 kali
		{
			unit(i, j);
		}
	}
}

void unit(int x, int y) //berikut data garis yang akan digambar untuk menggambarkan map.
{
	//Fungsi Membuat Map untuk Snake.
	if (x == 0 || y == 0 || x == gridX - 1 || y == gridY - 1) //Jika koordinat x dan y berada di bagian pinggri maka
	{
		glLineWidth(3.0); //Lebar garis adalah tiga
		glColor3f(1.0, 0.0, 0.0); //dan bewarna merah

	}
	else //jika tidak
	{
		glLineWidth(1.0); //lebar garis adalah satu
		glColor3f(1.0, 1.0, 1.0); //dan berwarna putih.
	}

	glBegin(GL_LINE_LOOP); //Bentuk grid atau map berasal dari primitiv line loop.
		glVertex3f(x, y, 0.0); //dengan koordinat berikut ini dan nantinya akan digambarkan berkali-kali pada fungsi drawGrid.
		glVertex3f(x + 1, y, 0.0);
		glVertex3f(x + 1, y + 1, 0.0);
		glVertex3f(x, y + 1, 0.0);
	glEnd();
}

void drawFood() //Fungsi menggambarkan makanan
{
	if (food) //Jika food sudah dimakan
		random(foodX, foodY); //Maka nanti akan digambarkan pada layar secara random
	food = false; //lalu food kembali diatur menjadi belum dimakan
	glColor3f(0.0, 0.0, 1.0); //Fungsi menentukan warna makanan
	glRectf(foodX,foodY, foodX + 1, foodY + 1); //fungsi menggambarkan makanan berbentuk persegi dengan koordinat titik sebagai berikut.
}

void drawSnake() //Fungsi menggambarkan snake.
{

	for (int i = snake_length - 1; i > 0; i--) //Algoritma perulangan untuk menentukan posisi tubuh snake
	{
		posX[i] = posX[i - 1]; //posisi badan sama dengan posisi kepala dikurangi satu
		posY[i] = posY[i - 1];
	}

	//Untuk mengatur arah kepala snake
	if (sDirection == UP) //Jika arah kepala ke atas
		posY[0]++; //maka posisi y bertambah
	else if (sDirection == DOWN) //arah kepala ke bawah
		posY[0]--; //posisi y berkurang
	else if (sDirection == RIGHT) //arah kepala ke kanan
		posX[0]++; //posisi x bertambah
	else if (sDirection == LEFT) //arah kepala ke kiri
		posX[0]--; //posisi x berkurang

	for (int i = 0; i < snake_length; i++) //untuk menggambarkan snake
	{
		if(i == 0) //menentukan warna kepala
			glColor3f(1.0, 0.0, 0.0);
		else //dan tubuh snake
			glColor3f(0.0, 1.0, 0.0);

		glRectd(posX[i], posY[i], posX[i] + 1, posY[i] + 1); //Fungsi menggambarkan setiap bagian snake
	}


	//Untuk mengecek apakah kepala snake mengenai batas map
	if (posX == 0 || posX[0] == gridX - 1 || posY[0] == 0 || posY[0] == gridY - 1)
	{
		gameOver = true;
	}

	//untuk mengecek apakah kepala snake mengenai makanan
	if (posX[0] == foodX && posY[0] == foodY)
	{
		score++; //score akan bertambah
		snake_length++; //ukuran tubuh snake juga bertambah
		if (snake_length > MAX) //namun akan berhenti bertambah ukuran ketika panjangnya sudah 60
		{
			snake_length = MAX;
		}
		food = true; //variabel food dijadikan true sehingga nanti dapat digambarkan kembali pada layar di posisi random
	}
}

void random(int& x, int& y) //Fungsi random untuk mengatur posisi makanan pada map
{
	int _maxX = gridX - 2;
	int _maxY = gridY - 2;
	int _min = 1;
	srand(time(NULL));
	x = _min + rand() % (_maxX - _min);
	y = _min + rand() % (_maxY - _min);

}